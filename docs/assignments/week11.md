# 11. Interface and Application programming

    requirements.txt: mkdocs-material<5.0
    gitlab-ci.yml: image: python:3.6

# Processing!

[https://processing.org/](https://processing.org/)

download, extract, navigate to processing.exe


<img src="../../images/week11/processing.jpg" width = 683px><br>

let's start simple:

    ellipse(10,10,10,10)

for a window let's add

    void setup(){
        size(800, 600);
    }

    void draw(){
        ellipse(10, 10, 10, 10);
    }

There are some predefinded variables like **mouseX** or **mouseY**

    void setup(){
        size(800, 600);
    }

    void draw(){
        ellipse(mouseX, mouseY, 50, 50);
    }

or
    ellipse(width/2, height/2, 50, 50);

if in doubt, open the "Referenz"

<img src="../../images/week11/processing_01.jpg" width = 683px><br>

    for (int i = 0; i < 40; i = i+1) {
      line(30, i, 80, i);
    }

    ```
    int animate = 0;
    float posX = 0;
    float posY = 0;

    void setup(){
      size(800, 600);
      frameRate(100);

    }

    void draw(){
      background(255);
      for(int i = 0; i<100; i=i+1){
        stroke(0);
        strokeWeight(1);
        line(i+1, 20*i, 850, 85*i);
      }
      float rx = random(-5, 5);
      float ry = random(-5, 5);
      posX = posX +rx;
      posY = posY +ry;
      stroke(#32a852);
      strokeWeight(5);
      fill(#d15217);
      ellipse(mouseX+posX , mouseY+posY, 50, 50);
      animate = animate +1;
      println(posX, posY);
    }
    ```

## Arduino to processing
Arduino:
    ```
    const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
    int sensorValue = 0;        // value read from the pot

    void setup() {
      // initialize serial communications at 9600 bps:
      Serial.begin(9600);
    }

    void loop() {
      // read the analog in value:
      sensorValue = analogRead(analogInPin);


      // print the results to the Serial Monitor:
      Serial.println(sensorValue);

      // wait 2 milliseconds before the next loop for the analog-to-digital
      // converter to settle after the last reading:
      delay(2);
    }
    ```

Processing:
    ```

    ```


# Python

[python datalogger](https://makersportal.com/blog/2018/2/25/python-datalogger-reading-the-serial-output-from-arduino-to-analyze-data-using-pyserial)

[tkinter tutorial](https://likegeeks.com/python-gui-examples-tkinter-tutorial/)

[https://jupyter.org/](https://jupyter.org/)
